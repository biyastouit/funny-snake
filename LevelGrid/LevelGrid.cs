public class LevelGrid   {

	private Vector2Int foodGridPosition;
	private int GridValue = 10;
	private int height;
	private int width;
	private Sprite foodSprite =GameAsset.instance.FoodSprite;
	GameObject foodGameObject;
	private Snake snake;
	public LevelGrid (int width, int height) {
		this.width = width;
		this.height = height;

	}
	public void Setup(Snake snake)
	{this.snake = snake;
		SpawnFood ();
	}

	// Update is called once per frame
	public void SpawnFood(){
		do
		{
			foodGridPosition = new Vector2Int (5+GridValue*Random.Range (-width, width - 1), 5+GridValue*Random.Range (-height, height-1));
		}
		while(foodGridPosition == snake.GetGridPosition());
		foodGameObject = new GameObject ("Food", typeof(SpriteRenderer));
		foodGameObject.GetComponent<SpriteRenderer> ().sprite = foodSprite;
		foodGameObject.GetComponent<SpriteRenderer> ().sortingOrder = 5;
		foodGameObject.transform.position = new Vector3 (foodGridPosition.x, foodGridPosition.y);
	}
	public bool SnakeMoved(Vector2Int snakeGridPosition){
		if(snakeGridPosition == foodGridPosition){
			Object.Destroy(foodGameObject);
				SpawnFood();
			Debug.Log("Eated");
			return true;
			}
		return false;
	}
	void Update () {

	}
}
